import csv
import argparse

"""
How to use
- Go to 'Alerts' in Azure portal
- Export to csv
- Run this script
"""

parser = argparse.ArgumentParser(
	prog='Azure Alert Output Parser',
	description='Parses the csv output of a specific alert query'
)
parser.add_argument('--filename', required=True)

args = parser.parse_args()


heartbeats = []
other = []
other_high_prio = []

with open(args.filename, 'r') as csv_file:
	reader = csv.reader(csv_file, delimiter=',')
	next(reader) # skip header

	for row in reader:
		# Totally not hardcoding this or anything, i'm lazy and no flexibility needed for now

		entry = {
			"name": row[0],
			"severity": row[1],
			"resource": row[2],
		}

		if 'heartbeat' in entry['name']:
			heartbeats.append(entry)
		elif entry['severity'].lower() == 'sev1':
			other_high_prio.append(entry)
		else:
			other.append(entry)

print('### heartbeats')
for entry in heartbeats:
	print(entry['resource'].split('/')[-1])

print('\n### other')
for entry in other:
	print(entry)

print('\n### other_high_prio')
for entry in other_high_prio:
	print(entry)

