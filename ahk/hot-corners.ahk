; Based on:
; AltEdge.ahk
; Sends Alt-Tab when the mouse is on the left edge of the screen.
; Keep it there to tab through the other windows. 
;Skrommel @ 2008

SetWinDelay 0
SetKeyDelay 0
CoordMode "Mouse","Screen"

tabbed := false

Loop
{
  MouseGetPos &mx,&my

  If (mx = 0 && my = 0)
  {
    If (tabbed = false)
    {
      Send "#{Tab}"
    }
    tabbed := true
  }
  Else
  {
    If (tabbed = true)
    {
      tabbed := false
    }
  }
  Sleep 50
}      


EXIT:
ExitApp