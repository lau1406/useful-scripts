// Run on https://export.microsoft365dsc.com/#Home
// Update identity to the id of the section (i.e. "SharePoint")
// How to use:
// Open https://export.microsoft365dsc.com/#Home
// Press F12 to open the developer tools
// Open the console
// Update 'identity' variable to the title of your section
// Copy paste into the console
// Profit
 
const identity = 'SharePoint'
let names = new Map()
 
const header = document.getElementById(identity)
const inputs = header.parentElement.getElementsByTagName('input')
 
for (let i=0; i < inputs.length; i++) {
    if (i==0) {continue}
 
    names.set(inputs[i].labels[0].children[1].innerHTML, inputs[i].checked)
}
 
let csv = ""
let keys = ""
let values = ""
 
for (let [key, value] of names) {
    keys += key + "\n"
    values  += value + "\n"
    csv  += key + "," + value + "\n"
}
 
console.log("keys:")
console.log(keys)
console.log("values:")
console.log(values)
console.log("combined:")
console.log(csv)