######################################################
#                                                    #
#  Simple script to logout of all Azure connections  #
#                                                    #
######################################################
param (
    [Parameter()]
    [switch]$Silent
)

if (!$Silent.IsPresent) {
    Write-Host "######################################################"
    Write-Host "#                                                    #"
    Write-Host "#  Simple script to logout of all Azure connections  #"
    Write-Host "#                                                    #"
    Write-Host "######################################################"
    Write-Host ""

    if (!(Get-AzContext)) {
        Write-Host "Not signed in, exiting!"
        Exit
    }
}


# Technically can run without the counting down, it's just a safety thing to prevent endless loops in case it cannot logout
$i = 50
while ((Get-AzContext) -and ($i -ge 0)) {
    if (!$Silent.IsPresent) {
        Write-Host "Logging out of:"
        Get-AzContext
    }
    $i--
    Disconnect-AzAccount | Out-Null
}
