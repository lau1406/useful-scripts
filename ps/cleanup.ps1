######################################################
#                                                    #
#         Simple script to do some cleanup           #
#                                                    #
######################################################
param (
    [Parameter()]
    [switch]$Silent
)

if (!$Silent.IsPresent) {
    Write-Host "######################################################"
    Write-Host "#                                                    #"
    Write-Host "#  Simple script to do some cleanup                  #"
    Write-Host "#                                                    #"
    Write-Host "######################################################"
    Write-Host ""
}


# Cleans temporary work directory
Remove-Item C:\Users\username\tmp_work_dir\* -Force -Recurse
